/*
 * @file WordParser.h
 * @authors Jim Daehn and <Your Name>
 * @brief WordParser is a utility class used to parse strings. Specifically,
 *        this utility will determine if a given string matches the language
 *        specified by the following grammar (see Exercise 6, pp. 184-185):
 *
 *        <word> = <dot>|<dash><word>|<word><dot>
 *        <dot>  = *
 *        <dash> = -
 *
 *        In essence, a legal string cannot contain dashes anywhere except at its beginning.
 */

#ifndef WORDPARSER_H_
#define WORDPARSER_H_

#include <string>

class WordParser {
private:
	static const char DOT = '.';
	static const char DASH = '-';
public:
	WordParser() {
    // inline implementation of default constructor
	}
  
	virtual ~WordParser() {
    // inline implementation of destructor
	}

  /**
   * Determine whether a given word matches the language described above.
   *
   * @param s the string under interrogation
   * @return True is returned if the given string matches the language; false otherwise.
   */
	bool isWord(std::string s);
};

#endif /* WORDPARSER_H_ */
