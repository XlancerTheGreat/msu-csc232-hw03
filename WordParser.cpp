/*
 * @file WordParser.cpp
 * @authors James R. Daehn and Joseph Sneddon
 * @brief Implementation of WordParser class.
 */

#include "WordParser.h"

bool WordParser::isWord(std::string s) {

	// Evaluating for base case: <dot>. (Short-circuit optimized)
  if ((s.length() == 1) && (s.front() == DOT)) {
		return true;

	// Evaluating for base case: <dash><word>. (Short-circuit optimized)
	} else if ((s.front() == DASH) && (isWord(s.erase(0,1)))) {
		return true;

	// Evaluating for base case: <word><dot>. (Short-circuit optimized)
	} else if ((s.back() == DOT) && (isWord(s.erase(s.length()-1, 1)))) {
		return true;

	// Returning false for cases: <emptyString>|<dash>|<word><dash>.
	} else {
		return false;
	}
}
